module gitlab.com/wryfi/solrizr

go 1.14

require (
	github.com/codegangsta/negroni v1.0.0
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.0
	github.com/stretchr/testify v1.3.0
	gopkg.in/yaml.v2 v2.2.4
)
