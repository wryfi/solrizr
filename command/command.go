package command

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/wryfi/solrizr/server"
	"gopkg.in/yaml.v2"
	"os"
	"runtime"
)


var cfgFile string
var SolrizrVersion, GitRevision, BuildDate string  // set at build-time via LDFLAGS (see Makefile)


func init() {
	cobra.OnInitialize(initConfig)
	RootCommand.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file")
	RootCommand.PersistentFlags().BoolP("debug", "d", false, "enable debug logs")
	RootCommand.AddCommand(versionCommand)
	RootCommand.AddCommand(serveCommand)
	RootCommand.AddCommand(configCommand)
}

func initConfig() {
	viper.SetDefault("listen", ":9999")
	viper.SetDefault("methods", []string{"GET", "HEAD"})
	viper.SetDefault("query_time_allowed", 10000)
	viper.SetDefault("max_rows", 1000)
	viper.SetDefault("upstream.protocol", "http")
	viper.SetDefault("upstream.host", "127.0.0.1")
	viper.SetDefault("upstream.port", 8983)
	viper.SetDefault("upstream.prefix", "/solr")
	viper.SetDefault("methods", []string{"GET", "HEAD"})
	viper.SetDefault("collection_endpoints", []string{"select", "query", "terms"})
	viper.SetDefault("parsers", []string{"lucene"})
	viper.SetDefault("parameters", []string{"q", "sort", "start", "rows",  "fq", "fl", "wt", "defType"})
	viper.SetDefault("aliases", []map[string]string{})
	viper.SetDefault("collections", []string{})
	viper.SetDefault("middlewares", []string{
		"MethodFilterMiddleware",
		"BodyFilterMiddleware",
		"AliasMiddleware",
		"ValidPathMiddleware",
		"UserInterfaceMiddleware",
		"ParameterMiddleware",
		"AdminFilterMiddleware",
		"UpdateFilterMiddleware",
		"LuceneLinterMiddleware",
	})
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		viper.SetConfigName("solrizr")
	}
	if err := viper.ReadInConfig(); err != nil {
		log.WithError(err).Warn("default configuration will be used")
	} else {
		log.WithFields(log.Fields{"file": viper.ConfigFileUsed()}).Info("reading configuration file")
	}
	viper.AutomaticEnv()
}


var RootCommand = &cobra.Command{
	Use: "solrizr",
	Short: "manage the solrizr proxy",
	Long: "solrizr is a reverse proxy that sanitizes requests to a solr backend",
}

var serveCommand = &cobra.Command{
	Use: "serve",
	Short: "run the solrizr server",
	Run: func(cmd *cobra.Command, args []string) {
		server.Serve()
	},
}

var configCommand = &cobra.Command{
	Use: "config",
	Short: "prints current solrizr configuration",
	Run: func(cmd *cobra.Command, args []string) {
		config := viper.AllSettings()
		var out []byte
		var err error
		if len(args) < 1 {
			out, err = yaml.Marshal(config)
			if err != nil {
				log.Errorf("could not unmarshal settings: %s", err)
				os.Exit(1)
			}
		} else {
			out, err = yaml.Marshal(config[args[0]])
			if err != nil {
				log.Errorf("could not unmarshal value for %v: %s", args[0], err)
				os.Exit(1)
			}
		}
		os.Stdout.WriteString(string(out))
	},
}

var versionCommand = &cobra.Command{
	Use: "version",
	Short: "display solrizr version information",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Version:          ", SolrizrVersion)
		fmt.Println("Git commit:       ", GitRevision)
		fmt.Println("Build date:       ", BuildDate)
		fmt.Println("Go build version: ", runtime.Version())
	},
}


func Execute() {
	if err := RootCommand.Execute(); err != nil {
		log.WithError(err).Fatalf("error executing root command")
	}
}
