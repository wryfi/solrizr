package server

import (
	"fmt"
	"os"
	log "github.com/sirupsen/logrus"
	"gitlab.com/wryfi/solrizr/config"
	"github.com/codegangsta/negroni"
	"net/http"
)


func Serve() {
	cfg := config.GetConfig()
	app := NewServer(cfg)
	server := &http.Server{
		Addr: cfg.Listen,
		Handler: app,
	}
	err := server.ListenAndServe()
	if err != nil {
		log.WithError(err).Fatal("error starting Proxy")
	}
	os.Exit(0)
}

func NewServer(cfg config.Config) *negroni.Negroni {
	app := negroni.New()
	for _, name := range cfg.Middlewares {
		if ware, err := GetMiddleware(name, cfg); err == nil {
			log.WithField("middleware", name).Debug("middleware enabled")
			app.Use(ware)
		}
	}
	app.UseHandler(NewMux(cfg))
	return app
}

func NewMux(cfg config.Config) *http.ServeMux {
	proxy := NewProxy(buildUpstream(cfg))
	mux := http.NewServeMux()
	mux.HandleFunc("/", proxy.Handler)
	return mux
}

func buildUpstream(cfg config.Config) string {
	return fmt.Sprintf("%s://%s:%d", cfg.Upstream.Protocol, cfg.Upstream.Host, cfg.Upstream.Port)
}