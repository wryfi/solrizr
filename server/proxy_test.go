package server

import (
	"bytes"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/wryfi/solrizr/config"
	"gitlab.com/wryfi/solrizr/misc"
	"net/http"
	"net/http/httptest"
	"testing"
)


type ProxyUnitTestSuite struct {
	suite.Suite
	config config.Config
}

func (suite *ProxyUnitTestSuite) SetupTest() {
	viper.SetConfigFile("../test.yml")
	if err := viper.ReadInConfig(); err != nil {
		suite.T().Fatal(err)
	}
	suite.config = config.GetConfig()
}

func (suite *ProxyUnitTestSuite) TestMethodFilterMiddleware() {
	request, err := http.NewRequest("POST", "/solr/solrizr/update", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusMethodNotAllowed, result.StatusCode, "POST should not be allowed")
	assert.Equal(suite.T(), "method not allowed\n", misc.ReadBody(result))
}

func (suite *ProxyUnitTestSuite) TestBodyFilterMiddleware() {
	request, err := http.NewRequest("GET", "/solr/solrizr/update", bytes.NewBufferString("hello solrizr"))
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusBadRequest, result.StatusCode, "request body not allowed")
	assert.Equal(suite.T(), "request body not allowed\n", misc.ReadBody(result))
}

func (suite *ProxyUnitTestSuite) TestValidPathMiddlewarePrefix() {
	request, err := http.NewRequest("GET", "/notsolr/solrizr/select", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusNotFound, result.StatusCode, "invalid prefix throws not found")
	assert.Equal(suite.T(), "resource not found\n", misc.ReadBody(result))
}

func (suite *ProxyUnitTestSuite) TestValidPathMiddlewareCollection() {
	viper.Set("collections", []string{"mycollection"})
	request, err := http.NewRequest("GET", "/solr/notmycollection/select", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusNotFound, result.StatusCode, "invalid collection throws not found")
	assert.Equal(suite.T(), "resource not found\n", misc.ReadBody(result))
	viper.Set("collections", suite.config.Collections)
}

func (suite *ProxyUnitTestSuite) TestValidPathMiddlewreEndpoint() {
	request, err := http.NewRequest("GET", "/solr/solrizr/rando", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusNotFound, result.StatusCode, "invalid endpoint throws not found")
	assert.Equal(suite.T(), "resource not found\n", misc.ReadBody(result))
}

func (suite *ProxyUnitTestSuite) TestUserInterfaceMiddleware() {
	request, err := http.NewRequest("GET", "/solr", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusNotFound, result.StatusCode, "interface should not be found")
	assert.Equal(suite.T(), "resource not found\n", misc.ReadBody(result))
}

func (suite *ProxyUnitTestSuite) TestUserInterfaceMiddlewareCss() {
	request, err := http.NewRequest("GET", "/solr/css/solr.css", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusNotFound, result.StatusCode, "interface CSS should not be found")
	assert.Equal(suite.T(), "resource not found\n", misc.ReadBody(result))
}

func (suite *ProxyUnitTestSuite) TestParameterMiddlewareParams() {
	request, _ := http.NewRequest("GET", "/solr/solrizr/select?foo=bar", nil)
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusOK, result.StatusCode, "invalid parameter request succeeds")
	assert.Equal(suite.T(), "", request.URL.Query().Get("foo"))
}

func (suite *ProxyUnitTestSuite) TestParameterMiddlewareParser() {
	request, _ := http.NewRequest("GET", "/solr/solrizr/select?defType=dismax&debug=query&timeAllowed=5&omitHeader=True", nil)
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusBadRequest, result.StatusCode, "non-lucene parser raises error")
	assert.Equal(suite.T(), "invalid parser\n", misc.ReadBody(result))
}

func (suite *ProxyUnitTestSuite) TestParameterMiddlewareRows() {
	request, _ := http.NewRequest("GET", "/solr/solrizr/select?q=*&rows=1000000", nil)
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusOK, result.StatusCode, "invalid rows request succeeds")
	assert.Equal(suite.T(), "1000", request.URL.Query().Get("rows"))
}

func (suite *ProxyUnitTestSuite) TestParameterMiddlewareTimeAllowed() {
	request, _ := http.NewRequest("GET", "/solr/solrizr/select?q=*&timeAllowed=1000000", nil)
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusOK, result.StatusCode, "invalid timeAllowed request succeeds")
	assert.Equal(suite.T(), "10000", request.URL.Query().Get("timeAllowed"))
}

func (suite *ProxyUnitTestSuite) TestParameterMiddlewareOmitHeader() {
	request, _ := http.NewRequest("GET", "/solr/solrizr/select?q=*&omitHeader=false", nil)
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusOK, result.StatusCode, "invalid omitHeader request succeeds")
	assert.Equal(suite.T(), "true", request.URL.Query().Get("omitHeader"))
}

func (suite *ProxyUnitTestSuite) TestAdminFilterMiddleware() {
	viper.Set("collections", []string{})
	request, err := http.NewRequest("GET", "/solr/admin/foo", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusNotFound, result.StatusCode, "admin should be hidden")
	assert.Equal(suite.T(), "resource not found\n", misc.ReadBody(result))
	viper.Set("collections", suite.config.Collections)
}

func (suite *ProxyUnitTestSuite) TestUpdateFilterMiddleware() {
	viper.Set("collections", []string{})
	request, err := http.NewRequest("GET", "/solr/solrizr/update", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusNotFound, result.StatusCode, "collection update should be hidden")
	assert.Equal(suite.T(), "resource not found\n", misc.ReadBody(result))
	viper.Set("collections", suite.config.Collections)
}


func (suite *ProxyUnitTestSuite) TestLuceneLinterCharacterFilter() {
	viper.Set("collections", []string{})
	request, err := http.NewRequest("GET", "/solr/solrizr/select/?q=[<>]", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusBadRequest, result.StatusCode, "bad chars should throw bad request")
	assert.Equal(suite.T(), "invalid character\n", misc.ReadBody(result))
	viper.Set("collections", suite.config.Collections)
}


func (suite *ProxyUnitTestSuite) TestLuceneLinterValidateAmpersand() {
	request, err := http.NewRequest("GET", "/solr/solrizr/select?q=title:foo%20%26%26", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusBadRequest, result.StatusCode, "tests use of ampersand operator")
	assert.Equal(suite.T(), "invalid use of && operator\n", misc.ReadBody(result))
}

func (suite *ProxyUnitTestSuite) TestLuceneLinterValidateBoostValue() {
	request, err := http.NewRequest("GET", "/solr/solrizr/select?q=title:foo^5", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusOK, result.StatusCode, "correct use of booster caret")
}

func (suite *ProxyUnitTestSuite) TestLuceneLinterValidateBoostValueInvalid() {
	request, err := http.NewRequest("GET", "/solr/solrizr/select?q=title:foo^f", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusBadRequest, result.StatusCode, "invalid boost value raises error")
	assert.Equal(suite.T(), "invalid boost field or value\n", misc.ReadBody(result))
}

func (suite *ProxyUnitTestSuite) TestLuceneLinterValidateBoostValueMissingField() {
	request, err := http.NewRequest("GET", "/solr/solrizr/select?q=title:^5", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusBadRequest, result.StatusCode, "invalid boost field raises error")
	assert.Equal(suite.T(), "invalid boost field or value\n", misc.ReadBody(result))
}

func (suite *ProxyUnitTestSuite) TestLuceneLinterValidateQuestionMark() {
	request, err := http.NewRequest("GET", "/solr/solrizr/select?q=%3ftitle:foo", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusBadRequest, result.StatusCode, "invalid qusestion mark raises error")
	assert.Equal(suite.T(), "? character must be preceded by alphanumeric\n", misc.ReadBody(result))
}

func (suite *ProxyUnitTestSuite) TestLuceneLinterValidateTildeValue() {
	request, err := http.NewRequest("GET", "/solr/solrizr/select?q=title:foo~f", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusBadRequest, result.StatusCode, "invalid tilde value raises error")
	assert.Equal(suite.T(), "~ character must be preceded by an alphanumeric and followed by a number\n",
		misc.ReadBody(result))
}

func (suite *ProxyUnitTestSuite) TestLuceneLinterValidateTildeKey() {
	request, err := http.NewRequest("GET", "/solr/solrizr/select?q=title:~0", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusBadRequest, result.StatusCode, "invalid tilde key raises error")
	assert.Equal(suite.T(), "~ character must be preceded by an alphanumeric and followed by a number\n",
		misc.ReadBody(result))
}

func (suite *ProxyUnitTestSuite) TestLuceneLinterValidateColons() {
	request, err := http.NewRequest("GET", "/solr/solrizr/select?q=title::foobar", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusBadRequest, result.StatusCode, "double colons raises error")
	assert.Equal(suite.T(), ": characters must separate fields from values with no space\n",
		misc.ReadBody(result))
}

func (suite *ProxyUnitTestSuite) TestLuceneLinterValidateBalance() {
	request, err := http.NewRequest("GET", "/solr/solrizr/select?q=title:(foobar", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusBadRequest, result.StatusCode, "unbalanced parens raises error")
	assert.Equal(suite.T(), "unbalanced parenthesis or brackets\n",
		misc.ReadBody(result))
}

func (suite *ProxyUnitTestSuite) TestLuceneLinterValidateUnbalancedQuote() {
	request, err := http.NewRequest("GET", "/solr/solrizr/select?q=title:%22", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusBadRequest, result.StatusCode, "unbalanced quote raises error")
	assert.Equal(suite.T(), "unbalanced or empty quotes\n",
		misc.ReadBody(result))
}

func (suite *ProxyUnitTestSuite) TestLuceneLinterValidateInvalidAND() {
	request, err := http.NewRequest("GET", "/solr/solrizr/select?q=AND%20title:mindfulness", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusBadRequest, result.StatusCode, "invalud use of AND raises error")
	assert.Equal(suite.T(), "invalid use of AND, OR, or AND NOT\n",
		misc.ReadBody(result))
}

// TestProxyUnitTestSuite hooks the testify suite into the testing framework
func TestProxyUnitTestSuite(t *testing.T) {
	suite.Run(t, new(ProxyUnitTestSuite))
}


type ProxyIntegrationTestSuite struct {
	suite.Suite
	config config.Config
}

func (suite *ProxyIntegrationTestSuite) SetupTest() {
	viper.SetConfigFile("../test.yml")
	if err := viper.ReadInConfig(); err != nil {
		suite.T().Fatal(err)
	}
	suite.config = config.GetConfig()
}

func (suite *ProxyIntegrationTestSuite) TestProxySolr() {
	request, err := http.NewRequest("GET", suite.config.Upstream.Prefix + "/solrizr/select", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	mux := NewMux(suite.config)
	mux.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusOK, result.StatusCode, "status code should be ok")
}

func (suite *ProxyIntegrationTestSuite) TestAliasMiddleware() {
	request, err := http.NewRequest("GET", "/search", nil)
	if err != nil {
		suite.T().Fatal(err)
	}
	response := httptest.NewRecorder()
	app := NewServer(suite.config)
	app.ServeHTTP(response, request)
	result := response.Result()
	assert.Equal(suite.T(), http.StatusOK, result.StatusCode, "alias should return ok")
	assert.Equal(suite.T(), "/solr/solrizr/select", request.URL.Path, "alias should rewrite path")
}

func TestProxyIntegrationTestSuite(t *testing.T) {
	suite.Run(t, new(ProxyIntegrationTestSuite))
}
