package server

import (
	"errors"
	"fmt"
	"github.com/codegangsta/negroni"
	log "github.com/sirupsen/logrus"
	"gitlab.com/wryfi/solrizr/config"
	"gitlab.com/wryfi/solrizr/misc"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)


var NotFound = "resource not found"

func GetMiddleware(name string, cfg config.Config) (negroni.Handler, error) {
	switch name {
	case "MethodFilterMiddleware":
		return &MethodFilterMiddleware{Middleware{name, cfg}}, nil
	case "BodyFilterMiddleware":
		return &BodyFilterMiddleware{Middleware{name, cfg}}, nil
	case "AliasMiddleware":
		return &AliasMiddleware{Middleware{name, cfg}}, nil
	case "ValidPathMiddleware":
		return &ValidPathMiddleware{Middleware{name, cfg}}, nil
	case "UserInterfaceMiddleware":
		return &UserInterfaceMiddleware{Middleware{name, cfg}}, nil
	case "ParameterMiddleware":
		return &ParameterMiddleware{Middleware{name, cfg}}, nil
	case "AdminFilterMiddleware":
		return &AdminFilterMiddleware{Middleware{name, cfg}}, nil
	case "UpdateFilterMiddleware":
		return &UpdateFilterMiddleware{Middleware{name, cfg}}, nil
	case "LuceneLinterMiddleware":
		return &LuceneLinterMiddleware{Middleware{name, cfg}}, nil
	default:
		return nil, errors.New("middleware not found")
	}
}


type Middleware struct {
	Name   string
	Config config.Config
}

type MethodFilterMiddleware struct {Middleware}

func (ware *MethodFilterMiddleware) ServeHTTP(response http.ResponseWriter, request *http.Request, next http.HandlerFunc) {
	if !misc.StringInSlice(request.Method, ware.Config.Methods) {
		http.Error(response, "method not allowed", http.StatusMethodNotAllowed)
		return
	}
	next.ServeHTTP(response, request)
}

type BodyFilterMiddleware struct {Middleware}

func (ware *BodyFilterMiddleware) ServeHTTP(response http.ResponseWriter, request *http.Request, next http.HandlerFunc) {
	if request.Body != nil {
		defer request.Body.Close()
		if body, err := ioutil.ReadAll(request.Body); err == nil {
			if len(body) > 0 {
				http.Error(response, "request body not allowed", http.StatusBadRequest)
				return
			}
	 	}
	}
	next(response, request)
}

type AliasMiddleware struct {Middleware}

func (ware *AliasMiddleware) ServeHTTP(response http.ResponseWriter, request *http.Request, next http.HandlerFunc) {
	for _, alias := range ware.Config.Aliases {
		if strings.HasPrefix(request.URL.Path, alias.From) {
			log.WithFields(log.Fields{"from": alias.From, "to": alias.To}).Debug("rewriting URL")
			request.URL.Path = strings.Replace(request.URL.Path, alias.From, alias.To, -1)
		}
	}
	if request.URL.Path == "/" {
		http.Error(response, NotFound, http.StatusNotFound)
		return
	}
	next.ServeHTTP(response, request)
}

type ValidPathMiddleware struct {Middleware}

func (ware *ValidPathMiddleware) ServeHTTP(response http.ResponseWriter, request *http.Request, next http.HandlerFunc) {
	prefix := misc.Deslash(fmt.Sprintf("/%s", ware.Config.Upstream.Prefix))
	if !strings.HasPrefix(request.URL.Path, prefix) {
		http.Error(response, NotFound, http.StatusNotFound)
		return
	}
	valid := regexp.MustCompile(fmt.Sprintf(`%s/(\w+)/(\w+)[^/]*`, prefix))
	if valid.MatchString(request.URL.Path) {
		matches := valid.FindStringSubmatch(request.URL.Path)
		if len(ware.Config.Collections) > 0 {
			if !misc.StringInSlice(matches[1], ware.Config.Collections) {
				http.Error(response, NotFound, http.StatusNotFound)
				return
			}
		}
		if len(ware.Config.CollectionEndpoints) > 0 {
			if !misc.StringInSlice(matches[2], ware.Config.CollectionEndpoints) {
				http.Error(response, NotFound, http.StatusNotFound)
				return
			}
		}
	} else {
		http.Error(response, NotFound, http.StatusNotFound)
		return
	}
	next(response, request)
}

type UserInterfaceMiddleware struct {Middleware}

func (ware *UserInterfaceMiddleware) ServeHTTP(response http.ResponseWriter, request *http.Request, next http.HandlerFunc) {
	if regexp.MustCompile(fmt.Sprintf(`^%s/*$`, ware.Config.Upstream.Prefix)).MatchString(request.URL.Path) {
		http.Error(response, NotFound, http.StatusNotFound)
		return
	}
	if regexp.MustCompile(fmt.Sprintf(`^%s/(?:css|js|libs)`, ware.Config.Upstream.Prefix)).MatchString(request.URL.Path) {
		http.Error(response, NotFound, http.StatusNotFound)
		return
	}
	next.ServeHTTP(response, request)
}

type ParameterMiddleware struct {Middleware}

func (ware *ParameterMiddleware) ServeHTTP(response http.ResponseWriter, request *http.Request, next http.HandlerFunc) {
	query := request.URL.Query()
	for key := range query {
		if len(ware.Config.Parameters) > 0 && !misc.StringInSlice(key, ware.Config.Parameters) {
			query.Del(key)
		}
	}
	if parser := query.Get("defType"); len(parser) > 0 && !misc.StringInSlice(parser, ware.Config.Parsers) {
		http.Error(response, "invalid parser", http.StatusBadRequest)
		return
	}
	if rows := query.Get("rows"); len(rows) > 0 {
		rows, _ := strconv.Atoi(rows)
		if rows < 0 || rows > ware.Config.MaxRows {
			query.Set("rows", strconv.Itoa(ware.Config.MaxRows))
		}
	}
	query.Set("timeAllowed", ware.Config.QueryTime)
	query.Set("omitHeader", "true")
	request.URL.RawQuery = query.Encode()
	next.ServeHTTP(response, request)
}

type AdminFilterMiddleware struct {Middleware}

func (ware *AdminFilterMiddleware) ServeHTTP(response http.ResponseWriter, request *http.Request, next http.HandlerFunc) {
	baseUrl := misc.Deslash(misc.BuildUrl(ware.Config, "admin"))
	if strings.HasPrefix(request.URL.Path, baseUrl) {
		http.Error(response, NotFound, http.StatusNotFound)
		return
	}
	next.ServeHTTP(response, request)
}

type UpdateFilterMiddleware struct {Middleware}

func (ware *UpdateFilterMiddleware) ServeHTTP(response http.ResponseWriter, request *http.Request, next http.HandlerFunc) {
	baseUrl := misc.Deslash(misc.BuildUrl(ware.Config))
	regex := regexp.MustCompile(fmt.Sprintf(`%s/.*/update`, baseUrl))
	if regex.MatchString(request.URL.Path) {
		http.Error(response, NotFound, http.StatusNotFound)
		return
	}
	next.ServeHTTP(response, request)
}

type LuceneLinterMiddleware struct{Middleware}

func (ware *LuceneLinterMiddleware) ServeHTTP(response http.ResponseWriter, request *http.Request, next http.HandlerFunc) {
	qstring := request.URL.Query().Get("q")
	if !ware.ValidateChars(qstring) {
		http.Error(response, "invalid character", http.StatusBadRequest)
		return
	}
	if !ware.ValidateBoost(qstring) {
		http.Error(response, "invalid boost field or value", http.StatusBadRequest)
		return
	}
	if !ware.ValidateAmpersand(qstring) {
		http.Error(response, "invalid use of && operator", http.StatusBadRequest)
		return
	}
	if !ware.ValidateQuestionMark(qstring) {
		http.Error(response, "? character must be preceded by alphanumeric", http.StatusBadRequest)
		return
	}
	if !ware.ValidateTilde(qstring) {
		http.Error(response, "~ character must be preceded by an alphanumeric and followed by a number", http.StatusBadRequest)
		return
	}
	if !ware.ValidateColons(qstring) {
		http.Error(response, ": characters must separate fields from values with no space", http.StatusBadRequest)
		return
	}
	if !ware.ValidatePlusMinus(qstring) {
		http.Error(response, "+/- must be followed by at least one alphanumeric", http.StatusBadRequest)
		return
	}
	if !ware.ValidateBalance(qstring) {
		http.Error(response, "unbalanced parenthesis or brackets", http.StatusBadRequest)
		return
	}
	if !ware.ValidateQuotes(qstring) {
		http.Error(response, "unbalanced or empty quotes", http.StatusBadRequest)
		return
	}
	if !ware.ValidateAndOrNot(qstring) {
		http.Error(response, "invalid use of AND, OR, or AND NOT", http.StatusBadRequest)
		return
	}
	next(response, request)
}

func (ware *LuceneLinterMiddleware) ValidateBoost(query string) bool {
	boostValue := regexp.MustCompile(`\^[^0-9]+`)
	if boostValue.MatchString(query) {
		return false
	}
	boostField := regexp.MustCompile(`(?:^|\s+|[^\p{L}0-9"'\]])\^`)
	if boostField.MatchString(query) {
		return false
	}
	return true
}

func (ware *LuceneLinterMiddleware) ValidateChars(query string) bool {
	rejects := regexp.MustCompile(`[^\p{L}0-9_+\-:.()\"*?&|!{}\[\]\^~\\@#\/$%'= ]`)
	if match := rejects.FindString(query); len(match) > 0 {
		return false
	}
	return true
}

func (ware *LuceneLinterMiddleware) ValidateAmpersand(query string) bool {
	doubleAmpsMatch := regexp.MustCompile(`^([\p{L}0-9_+\-:.()\"*?&|!{}\[\]\^~\\@#\/$%'=]+( && )?[\p{L}0-9_+\-:.()\"*?|!{}\[\]\^~\\@#\/$%'=]+[ ]*)+$`)
	if regexp.MustCompile(`[&]{2}`).MatchString(query) {
		if !doubleAmpsMatch.MatchString(query) {
			return false
		}
	}
	return true
}

func (ware *LuceneLinterMiddleware) ValidateQuestionMark(query string) bool {
	qmarkMatch := regexp.MustCompile(`^(\?)|([^\p{L}0-9_+\-:.()\"*?&|!{}\[\]\^~\\@#\/$%'=]\?+)`)
	if qmarkMatch.MatchString(query) {
		return false
	}
	return true
}

func (ware *LuceneLinterMiddleware) ValidateTilde(query string) bool {
	invalidTilde := regexp.MustCompile(`(?:[^\p{L}0-9*?]~[0-9]+)|(?:[\p{L}0-9?*]+~[^0-9]|[^\p{L}0-9*?]~[^0-9])`)
	if invalidTilde.MatchString(query) {
		return false
	}
	return true
}

func (ware *LuceneLinterMiddleware) ValidateColons(query string) bool {
	// [^\\\s]:[\s] matches a character followed by a colon and a space
	// [^\\\s]:$  matches a character followed by a colon and EOL
	// [\s][^\\]?: matches a space followed by 0 or 1 chars and a colon
	// ^[^\\\s]?: matches a line start then 0 or 1 chars and a colon
	// :: matches two colons
	invalidColons := regexp.MustCompile(`[^\\\s]:[\s]|[^\\\s]:$|[\s][^\\]?:|^[^\\\s]?:|::`)
	if invalidColons.MatchString(query) {
		log.Debugf("matched %s", invalidColons)
		return false
	}
	return true
}

func (ware *LuceneLinterMiddleware) ValidatePlusMinus(query string) bool {
	validPlusMinus := regexp.MustCompile(`^[^\n+\-]*$|^([+-]?[\p{L}0-9_:.()\"*?&|!{}\[\]\^~\\@#\/$%'=]+[ ]?)+$`)
	if !validPlusMinus.MatchString(query) {
		return false
	}
	return true
}

func (ware *LuceneLinterMiddleware) ValidateBalance(query string) bool {
	parensOpen := regexp.MustCompile(`\(`).FindAllString(query, -1)
	parensClose := regexp.MustCompile(`\)`).FindAllString(query, -1)
	if len(parensOpen) != len(parensClose) {
		return false
	}
	bracketsOpen := regexp.MustCompile(`\[`).FindAllString(query, -1)
	bracketsClose := regexp.MustCompile(`\]`).FindAllString(query, -1)
	if len(bracketsOpen) != len(bracketsClose) {
		return false
	}
	return true
}

func (ware *LuceneLinterMiddleware) ValidateQuotes(query string) bool {
	if quotes := regexp.MustCompile(`"`).FindAllString(query, -1); (len(quotes) % 2) > 0 {
		return false
	}
	if match := regexp.MustCompile(`""`).FindAllString(query, -1); len(match) > 0 {
		return false
	}
	return true
}

func (ware *LuceneLinterMiddleware) ValidateAndOrNot(query string) bool {
	if andornot := regexp.MustCompile(`AND|OR|NOT`).MatchString(query); andornot == true {
		test1 := regexp.MustCompile(`^([\p{L}0-9_+\-:.()\"*?&|!{}\[\]\^~\\@\/#$%'=]+\s*((AND )|(OR )|(AND NOT )|(NOT ))?[\p{L}0-9_+\-:.()\"*?&|!{}\[\]\^~\\@\/#$%'=]+[ ]*)+$`)
		if !test1.MatchString(query) {
			return false
		}
		test2 := regexp.MustCompile(`^((AND )|(OR )|(AND NOT )|(NOT ))|((AND)|(OR)|(AND NOT )|(NOT))[ ]*$`)
		if test2.MatchString(query) {
			return false
		}
	}
	return true
}