package server

import (
	log "github.com/sirupsen/logrus"
	"net/http"
	"net/http/httputil"
	"net/url"
)

type Proxy struct {
	Target *url.URL
	Proxy  *httputil.ReverseProxy
}

func (proxy *Proxy) Handler(response http.ResponseWriter, request *http.Request) {
	request.Host = proxy.Target.Host
	log.WithFields(log.Fields{"url": request.URL, "host": request.Host}).Debug("requesting upstream data")
	proxy.Proxy.ServeHTTP(response, request)
}

func NewProxy(target string) *Proxy {
	targetUrl, err := url.Parse(target)
	if err != nil {
		log.WithError(err).Fatalf("error parsing Target url")
	}
	httpProxy := httputil.NewSingleHostReverseProxy(targetUrl)
	httpProxy.ModifyResponse = func(response *http.Response) error {
		response.Header.Add("X-Powered-By", "solrizr")
		return nil
	}
	proxy := &Proxy{
		Target: targetUrl,
		Proxy:  httpProxy,
	}
	return proxy
}