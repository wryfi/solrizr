package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/wryfi/solrizr/command"
)

func main() {
	command.Execute()
}

func init() {
	log.SetLevel(log.DebugLevel)
}
