# solrizr

solrizr is a reverse proxy purpose-built for solr. Written in
golang for performance and portability, solrizr sanitizes solr/lucene
queries before they hit your solr backends, for improved security and
reliability.

solrizr uses the reverse proxy features built into `net/http`, with
[negroni](https://github.com/urfave/negroni) as the middleware engine.

solrizr is *not* a load balancer (you can only proxy to one backend solr
node at a time). Proper load balancing would add a lot of complication
that is better handled by ... an actual load balancer. Put solrizr in
front of one or behind one as fits your purpose.

## Features

* protects solr against common threats and errors
* configurable middlewares (see below)
* flexible configuration system (built on viper)
* cross-platform and performant
* reasonable test coverage (always improving)
* single-binary installation

## Quick Start

For a quick start, you can download the latest binary release and run
`solrizr serve -c solrizr.yml`, where `solrizr.yml` might look like what
you'll find in [test.yml](test.yml). You can view solrizr's
configuration by running `solrizr config` (with no other arguments this
will print the defaults).

The default configuration leaves the value of `collections` unset, as
solrizr has no way of knowing what collections you host. It's strongly
recommended to set this configuration value to the list of collections
you want solrizr to proxy.

Note that the ordering of middlewares can matter in some cases, so try
to stick to the default ordering as you add or remove middlewares.

## Notes on solr security

The default configuration of `solrizr` should protect against many
common threats. Modifying the defaults may reduce your security in some
cases, and you should understand the implications of doing so.

In particular, the following configurations are important to securing
solr:

* blocking `POST`, `PUT`, `PATCH`, and `DELETE` methods via the Method
  Filter Middleware
* disabling `stream.body`, `stream.url`, and `qt` parameters via
  Parameter Middleware
* blocking access to update and admin URLs via Admin Filter Middleware
  and Update Filter Middleware
* blocking access to the point-and-click browser interface and its
  components via the User Interface Middleware
* blocking request bodies

## Make Commands

There are a handful of `make` commands included in the toplevel
`Makefile` for convenience. Some of these require `docker` and `curl` to
be installed on your system.

* `make test` - run unit tests
* `make integration` - run integration tests against solr in docker
* `make solr` - bring up a solr container with a test core and data
* `make solr-start` - just start a container, no data
* `make solr-stop` - stop any running solr containers
* `make bin` - generate a binary under `bin/`
* `make release` - generate release with ZIPs and hashes
* `make clean` - stop docker and clean everything up


## Middlewares

Middlewares are run via negroni and can be enabled or disabled via their
listing in the configuration. All middlewares are enabled by default,
but some of them require specific configuration values to be operable.

### Method Filter Middleware

This middleware blocks HTTP methods that you don't want to allow to your
backend. It works by reading a list of allowed methods from your
configuration, e.g. the default values, which allow only `GET` and
`HEAD` requests:

```
methods:
- GET
- HEAD
```

Any attempt to call an unlisted method will be denied with an http/405.
An empty methods list in the configuration is equivalent to disabling
the middleware.

### Body Filter Middleware

Rejects requests that have a body with http/400.

solr (somewhat oddly) reads the body of GET requests, allowing users to
format query parameters as json via GET.

solrizr doesn't validate request bodies, and allowing the default
behavior can circumvent some protections implemented by the
`ParameterMiddleware`.


### Alias Middleware

This middleware creates simple aliases by rewriting the request URL
before sending it to the proxy. It's a fairly naive implementation using
string replacement, so don't expect anything fancy.

The following configuration will match incoming requests for `/search`
and replace them with `/solr/solrizr/select` when sending to the
backend.

```
aliases:
- from: /search
  to: /solr/solrizr/select
```

### Valid Path Middleware

The valid path middleware attempts to validate the URL path of a request
before sending it to solr. It validates that:

* the request path matches the configured upstream prefix (by default
  `/solr`)
* the request path is in the format of
  `/{prefix}/{collection}/{endpoint}`
* if the list of collections in the configuration is not empty, the
  collection is among them
* if the list of endpoints in the configuration is not empty, the
  endpoint is among them

### User Interface Middleware

Blocks access to the point-and-click solr management interface and all
of its accompanying assets.

### Parameter Middleware

The parameter middleware sanitizes the URL request parameters that form
solr queries. It validates that:

* each URL parameter is in the configured list of allowed
  parameters, if the list isn't empty
  * if the parameter is not in the approved list, it is silently dropped
* the parser specified in `defType`, if any, is in the list of
  configured parsers (by default limited to `lucene`)
* the requested number of rows does not exceed the maximum
  configured value
* the `timeAllowed` parameter is set according to the configuration
* the `omitHeader` parameter is set to `true`

### Admin Filter Middleware

Blocks access to the solr `/admin` endpoint.

### Update Filter Middleware

Blocks access to the `/update` endpoint on collections.

### Lucene Linter Middleware

This middleware attempts to lint lucene queries to prevent egregiously
bad ones from making their way to solr.

Largely based on a series of regexes adapted from
[lucene-query-validator](https://github.com/praized/lucene-query-validator),
the quality of which vary, more test coverage is needed here.

The middleware endeavors to reject any of the following:

* invalid characters in the query
* use of carets that are invalid as boost values
* invalid use of `&&`
* `?` not preceded by an alphanumeric
* `~` not preceded by an alphanumeric and followed by a number
* various invalid uses of colons
* `+` and `-` followed by at least one alphanumeric
* unbalanced parenthesis
* unbalanced or empty quotes
* invalid use of `AND`, `OR`, or `AND NOT`

A (probably outdated) overview of lucene query syntax can be found
[here](https://lucene.apache.org/core/2_9_4/queryparsersyntax.html).

## Acknowledgements

While solrizr is a novel project, it derives insipration from:

* [lucene-query-validator](https://github.com/praized/lucene-query-validator)
* [solr-security-proxy](https://github.com/dergachev/solr-security-proxy)
* [n2proxy](https://github.com/txn2/n2proxy)

Thank you to the contributors to the above projects for your work!
