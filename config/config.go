package config

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type Config struct {
	Upstream    Upstream
	Listen      string
	QueryTime   string   `mapstructure:"query_time_allowed,omitempty"`
	MaxRows		int  	 `mapstructure:"max_rows,omitempty"`
	Aliases     []Alias
	Collections []string
	Middlewares []string
	Methods 	[]string
	Parameters  []string
	CollectionEndpoints []string `mapstructure:"collection_endpoints"`
	Parsers []string
}

type Upstream struct {
	Protocol 	string
	Host 		string
	Port 		int
	Prefix 		string
}

type Alias struct {
	From 		string
	To 			string
}


func GetConfig() (config Config) {
	if err := viper.Unmarshal(&config); err != nil {
		log.WithError(err).Fatal("error parsing configuration")
	}
	return
}
