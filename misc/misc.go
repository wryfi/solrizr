package misc

import (
	"gitlab.com/wryfi/solrizr/config"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
)

// If query appears in list, return true; otherwise return false.
func StringInSlice(query string, list []string) bool {
	for _, listStr := range list {
		if listStr == query {
			return true
		}
	}
	return false
}

func ReadBody(response *http.Response) string {
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return ""
	}
	return string(body)
}

func BuildUrl(cfg config.Config, parts ...string) string {
	baseUrl := "/" + cfg.Upstream.Prefix
	path := strings.Join(parts, "/")
	return strings.Join([]string{baseUrl, path}, "/")
}

func Deslash(slashy string) string {
	return regexp.MustCompile(`/{2,}`).ReplaceAllString(slashy, "/")
}
