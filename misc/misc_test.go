package misc

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"testing"
)


type MiscUnitTestSuite struct {
	suite.Suite
}


func (suite *MiscUnitTestSuite) TestStringInSlice() {
	assert.Equal(suite.T(), true, StringInSlice("poiu", []string{"asdf", "poiu"}))
}

func (suite *MiscUnitTestSuite) TestStringNotInSlice() {
	assert.Equal(suite.T(), false, StringInSlice("asdf", []string{"qwer", "yuio"}))
}

func (suite *MiscUnitTestSuite) TestStringInEmptySlice() {
	assert.Equal(suite.T(), false, StringInSlice("asdf", []string{}))
}

func TestMiscUnitTestSuite(t *testing.T) {
	suite.Run(t, new(MiscUnitTestSuite))
}