# Need to set this for some Linux installs to use bash builtins like pushd
SHELL := /bin/bash

# Override this env var when building release
SOLRIZR_VERSION?=$(shell git describe --tags --dirty || echo -n "0.0.0")
SOLRIZR_GIT_REV?=$(shell git rev-parse --short HEAD)
SOLRIZR_BUILD_DATE?=$(shell date -u +'%FT%T%z')

LDFLAGS=-ldflags "-X gitlab.com/wryfi/solrizr/command.SolrizrVersion=$(SOLRIZR_VERSION) \
				 -X gitlab.com/wryfi/solrizr/command.GitRevision=$(SOLRIZR_GIT_REV) \
				 -X gitlab.com/wryfi/solrizr/command.BuildDate=$(SOLRIZR_BUILD_DATE) -w -s" main.go

.PHONY: all
all: test bin

bin: clean
	go build -o bin/solrizr $(LDFLAGS)

release:
	GOARCH=amd64 GOOS=darwin go build -o build/solrizr_$(SOLRIZR_VERSION)_darwin_amd64 $(LDFLAGS)
	pushd build && shasum -a 256 solrizr_$(SOLRIZR_VERSION)_darwin_amd64 > sha256sums_$(SOLRIZR_VERSION).txt && popd
	pushd build && mv solrizr_$(SOLRIZR_VERSION)_darwin_amd64 solrizr && popd
	pushd build && zip solrizr_$(SOLRIZR_VERSION)_darwin_amd64.zip solrizr && rm solrizr && popd
	pushd build && shasum -a 256 solrizr_$(SOLRIZR_VERSION)_darwin_amd64.zip >> sha256sums_$(SOLRIZR_VERSION).txt && popd
	GOARCH=amd64 GOOS=linux go build -o build/solrizr_$(SOLRIZR_VERSION)_linux_amd64 $(LDFLAGS)
	pushd build && shasum -a 256 solrizr_$(SOLRIZR_VERSION)_linux_amd64 >> sha256sums_$(SOLRIZR_VERSION).txt && popd
	pushd build && mv solrizr_$(SOLRIZR_VERSION)_linux_amd64 solrizr
	pushd build && zip solrizr_$(SOLRIZR_VERSION)_linux_amd64.zip solrizr && rm solrizr && popd
	pushd build && shasum -a 256 solrizr_$(SOLRIZR_VERSION)_linux_amd64.zip >> sha256sums_$(SOLRIZR_VERSION).txt && popd

.PHONY: test
test:
	go test -v -run '.*UnitTestSuite' ./...

.PHONY: integration
integration: solr
	go test -v -run '.*IntegrationTestSuite' ./...

.PHONY: clean
clean: solr-stop
	rm -rf bin/ build/

.PHONY: solr
solr: solr-start
	docker exec -it --user=solr solrizrtest solr create_core -c solrizr
	curl 'http://localhost:8983/solr/solrizr/update/json?commit=true' --data-binary @test-data.json -H 'Content-Type: application/json'

.PHONY: solr-start
solr-start: solr-stop
	docker run --rm -d --name solrizrtest -p 8983:8983 solr:7.7

.PHONY: solr-stop
solr-stop:
	-docker stop solrizrtest
